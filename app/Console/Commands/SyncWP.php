<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\User;

class SyncWP extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SyncWP:syncwp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync users with wordpress';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$username = "info@prodes.nl";
    	$password = "AB2viRKsV#*9Y";
    	$url = "https://www.nvlknf.nl/wp-json/";

	    $client = new Client(); //GuzzleHttp\Client
	    $result = $client->post($url.'jwt-auth/v1/token', [
		    'form_params' => [
			    'username' => $username,
			    'password' => $password
		    ]
	    ]);
		
		$resultsobj = json_decode($result->getBody());
		$this->info($resultsobj->token);

	    $headers = [
		    'Authorization' => 'Bearer ' . $resultsobj->token,
		    'Accept'        => 'application/json',
	    ];

		$hasresponse = true;
		$i = 0;
	    while ($hasresponse) {
	    	$i++;
		    $userresponse = $client->request( 'GET', $url.'wp/v2/users?page='.$i.'&context=edit&per_page=100&roles=subscriber', [
			    'headers' => $headers
		    ] );

		    $usersobj = json_decode($userresponse->getBody());

			foreach($usersobj as $key => $user) {
				if($user->email !== "") {
					$existinguser = User::where( "email", $user->email )->first();
					if ( $existinguser == null ) {
						User::create( [
							'name'     => $user->username,
							'email'    => $user->email,
							'password' => bcrypt( substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", mt_rand( 0, 51 ), 1 ) . substr( md5( time() ), 1 ) ),
							'role'     => 'gebruiker'
						] );
					}
				}
				else {
					$this->info($user->id.' Does not have an email!?');
				}
			}
			if(empty($usersobj)) {
				$hasresponse = false;
			}
	    }
    }
}
