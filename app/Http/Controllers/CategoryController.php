<?php

namespace App\Http\Controllers;

use App\QuizQuestionCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller {
	public function index() {
		$categories = QuizQuestionCategory::all();

		return view( 'category.index', [ 'categories' => $categories ] );
	}

	public function create() {
		return view( 'category.create' );
	}

	public function store( Request $request ) {
		$surFileName = $request->input('SurvivaltxtFileName');
		$txtdescription  = array();
		$images  = array();

		$category = QuizQuestionCategory::create([
			'name' => $request->get( 'name' ),
			'cat_description' => $request->get( 'description' ),
		]);

		//dd($request);
		if ( ! is_null( $request->file('image'))) {
			$files = $request->file('image');
			foreach($files as $file){
				$name = $file->getClientOriginalName();
				for ($i=0; $i < count($surFileName); $i++) {
					if ($name == $surFileName[$i]){
						$getFilename = $file->getFilename();
						$file->move(public_path( 'storage/category_images/' . $category->id ), $name);
						$images[] = $name;
					}
				}
			}

			$txtdescription = $request->get( 'txtdescription' );
		}
		else {
			$image = array();
			$txtdescription = array();
		}

		$category->update([
			'cat_image'             => json_encode($images),
			'cat_image_description' => json_encode($txtdescription),
		]);
		$images = [];

		return redirect()->route( 'category.index' );
	}

	public function edit( $id ) {
		$category = QuizQuestionCategory::where( 'id', $id )->first();
		return view( 'category.edit', [ 'category' => $category ] );
	}

	public function update( Request $request, $id ) {
		$category = QuizQuestionCategory::where( 'id', $id )->first();
		$surFileName = $request->input('SurvivaltxtFileName');
		$imageold  = array();
		$image  = array();

		if ( ! is_null($request->file('image'))) {
			$files = $request->file('image');
			foreach ($files as $file) {
				$name = $file->getClientOriginalName();
				for ($i = 0; $i < count($surFileName); $i++) {
					if ($name == $surFileName[$i]) {
						$getFilename = $file->getFilename();
						$file->move(public_path( 'storage/category_images/' . $category->id ), $name);
						$imageold[] = $name;
					}
				}
			}

			foreach (json_decode($category['cat_image']) as $key) {
				for ($i=0; $i < count($surFileName); $i++) {
					if($key == $surFileName[$i]){
						$image[] = $key;
					}
				}
			}
			$imageold = array_merge($image, $imageold);
		}else{
			if(!empty($surFileName)){
				$imageold = $surFileName;
			}else{
				$imageold = array();
				$txtdescription = array();
			}
		}

		$category->cat_image = json_encode($imageold);
		$category->cat_image_description = json_encode($request->get( 'txtdescription' ));
		$category->name = $request['name'];
		$category->cat_description = $request['description'];

		$category->save();

		return redirect()->route( 'category.index' );
	}

	public function cat_image_save( Request $request) {
		$file = $request->file('cat_image');
		echo $file;
	}

	public function deleteCategory( Request $request, $id ) {
		QuizQuestionCategory::find($id)->delete();
		return redirect()->route( 'category.index' );
	}
}
