<?php

namespace App\Http\Controllers;

use App\Mail\NewQuizAvailible;
use App\QuizAnswer;
use App\QuizQuestion;
use App\QuizQuestionAnswer;
use App\QuizQuestionCategory;
use App\QuizSubmission;
use App\QuizUser;
use App\QuizResult;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\User;
use App\Quiz;
use Illuminate\Support\Facades\Auth;
use Mail;
use DB;
use Excel;

class QuizController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$activeQuizzes   = Quiz::orderBy( 'expires_at', 'ASC' )->active()->get();
		$inactiveQuizzes = Quiz::inactive( 'expires_at', 'DESC' )->get();
		return view( 'quiz.index', compact( 'activeQuizzes', 'inactiveQuizzes' ) );
	}

	public function create() {
		$categories = QuizQuestionCategory::pluck('name', 'id')->all();
		return view( 'quiz.create', ['categories' => $categories]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		$Quiz = Quiz::create( [
			'title'       =>  $request->get( 'title' ),
			'description' =>  $request->get( 'description' ),
			'start'       => empty($request->get( 'start' )) ? null : Carbon::parse($request->get( 'start' )),
			'expires_at'  => empty($request->get( 'expires_at' )) ? null : Carbon::parse($request->get( 'expires_at' )),
		] );
		$Quiz->save();

		foreach ( $request->get( 'question' ) as $question_key => $question ) {
			$image  = array();
			$txtdescription  = array();
			$points = $request->get( 'points' )[ $question_key ];
			$description = $request->get('questiondescription')[ $question_key ];
			$surFileName = $request->input('SurvivaltxtFileName')[ $question_key ];

			$category = $request->get('category')[ $question_key ];

			if ( ! is_null( $request->file( 'image' )[ $question_key ] ) || $request->get('txtdescription')[ $question_key ]) {
				if($files = $request->file('image')[ $question_key ]){

			        foreach($files as $file){
			            $name = $file->getClientOriginalName();
			            for ($i=0; $i < count($surFileName); $i++) {
			            	if($name == $surFileName[$i]){
				            	$getFilename = $file->getFilename();
				            	$file->move(public_path( 'storage/quiz/' . $Quiz->id ), $name );
				            	$image[] = $name;
				            }
			            }
			        }

			    }
			    $txtdescription = $request->get( 'txtdescription' )[ $question_key ];
			}
			else {
				$image = array();
				$txtdescription = array();
			}

			/*if ( ! is_null( $request->file( 'image' )[ $question_key ] ) ) {
				$request->file( 'image' )[ $question_key ]->move( public_path( 'storage/quiz/' . $Quiz->id ), $request->file( 'image' )[ $question_key ]->getFilename() . '.' . $request->file( 'image' )[ $question_key ]->getClientOriginalExtension() );

				$image = $request->file( 'image' )[ $question_key ]->getFilename() . '.' . $request->file( 'image' )[ $question_key ]->getClientOriginalExtension();
			}
			else {
				$image = null;
			}*/

			$question = QuizQuestion::create( [
				'quiz_id'  => $Quiz->id,
				'question' => $question,
				'description' => $description,
				'question_cat_id' => $category,
				'imagedescription' => json_encode($txtdescription),
				'image'    => json_encode($image),
				'points'   => $points
			] );

			$question->save();

			foreach ( $request->get( 'answer' )[ $question_key ] as $key => $answer ) {
				$correct_answer_key = $request->get( 'correct_answer' )[ $question_key ];
				if ( $answer != '' ) {
					$quizanswer = QuizQuestionAnswer::create( [
						'quiz_question_id' => $question->id,
						'answer'           => $answer,
						'correct'          => ( $key == $correct_answer_key ) ? true : false
					] );
					$quizanswer->save();
				}
			}
		}

		$users = User::where( "role", "gebruiker" );

		$users->each(function ($item) use($Quiz) {

			$bytes = random_bytes(5);
			$quizuser = QuizUser::create( [
				'quiz_id' => $Quiz->id,
				'user_id' => $item->id,
				'code'    => bin2hex($bytes)
				]);

			$quizuser->save();
//			Mail::to($item->email)->send(new NewQuizAvailible($quizuser));

		});

		return redirect()->route( 'quiz.index' );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $id ) {
		if(!$request->user()->hasRole('beheerder')) {
			$quizuser   = QuizUser::where( 'user_id', $request->user()->id )
			                      ->where( 'quiz_id', $id );
			$submission = QuizSubmission::where( 'user_id', $request->user()->id )->where( 'quiz_id', $id );

			if ( ! $submission->get()->isEmpty() || $quizuser->get()->isEmpty() ) {
				return redirect()->route( 'quiz.login' );
			}
		}
		$quiz = Quiz::where( 'id', $id )->first();
		$ordered_quiz = [];
		foreach($quiz->QuizQuestion()->get() as $index => $question ) {
			$ordered_quiz[QuizQuestionCategory::where('id', $question->question_cat_id)->first()->name][] = $question;
		}
//		dd($ordered_quiz);

		return view( 'quiz.show', [ 'Quiz' => $quiz, 'questions' => $ordered_quiz] );
	}

	/**
	 * Results from the painters.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function results( $id ) {
		$results = User::with( 'QuizSubmissions' )->where('role', 'gebruiker')->get();
		$quiz    = Quiz::where( 'id', $id )->first();

		return view( 'quiz.results', [ 'users' => $results, 'quiz' => $quiz ] );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		$categories = QuizQuestionCategory::pluck('name', 'id')->all();
		return view( 'quiz.edit', [ 'Quiz' => Quiz::where( 'id', $id )->first(), 'categories' => $categories ] );
	}

	public function delete( $id ) {
		$quiz = Quiz::where( 'id', $id )->first();

		if ( $quiz ) {
			$quiz->delete();
		}

		return redirect()->route( 'quiz.index' )->with( 'message', 'De quiz is verwijderd' );
	}


	public function update( Request $request, $id ) {
		$Quiz = Quiz::where( 'id', $id )->first();

		$Quiz->update( [
			'title'       => $request->get('title'),
			'description' => $request->get('description'),
			'start'       => empty($request->get( 'start' )) ? null : Carbon::parse($request->get( 'start' )),
            'expires_at'  => empty($request->get( 'expires_at' )) ? null : Carbon::parse($request->get( 'expires_at' )),
		] );

		// handle newly added questions.
		if ( ! is_null( $request->get('newquestion'))) {
			foreach ($request->get('newquestion') as $question_key => $question) {
				$image  = array();
				$points = $request->get('newpoints')[$question_key];
				$category = $request->get('newquestioncategory')[$question_key];
				$description = $request->get('newquestiondescription')[$question_key];
				$surFileName = $request->input('SurvivaltxtFileName')[$question_key];
				//$imagedescription = (array_key_exists($question_key, $request->get('newimage_comment'))) ? $request->get('newimage_comment')[ $question_key ] : null;

                if( ! is_null($request->file('newimage')) || $request->get('txtdescription')[ $question_key ]) {
                    if (array_key_exists($question_key, $request->file('newimage'))) {
                    	if($files = $request->file('newimage')[ $question_key ]){
							foreach($files as $file){
					            $name = $file->getClientOriginalName();
					            for ($i=0; $i < count($surFileName); $i++) {
			            			if($name == $surFileName[$i]){
							            $getFilename = $file->getFilename();
							            $file->move(public_path( 'storage/quiz/' . $Quiz->id ), $name );
							            $image[] = $name;
						            }
			            		}
					        }
					    }

                        /*$request->file('newimage')[$question_key]->move(public_path('storage/quiz/' . $Quiz->id), $request->file('newimage')[$question_key]->getFilename() . '.' . $request->file('newimage')[$question_key]->getClientOriginalExtension());

                        $image = $request->file('newimage')[$question_key]->getFilename() . '.' . $request->file('newimage')[$question_key]->getClientOriginalExtension();*/
                    }
                    $txtdescription = $request->get( 'txtdescription' )[ $question_key ];
                }
                else{
					$image = array();
					$txtdescription = array();
				}

				$question = QuizQuestion::create([
					'quiz_id' => $Quiz->id,
					'question' => $question,
					'description' => $description,
					'question_cat_id' => $category,
					'image' => json_encode($image),
					'imagedescription' => json_encode($txtdescription),
					'points' => $points,
				]);

				foreach ($request->get('answer')[$question_key] as $key => $answer) {
					if ( $answer != '' ) {
						//dd($answer);
						$quizanswer = QuizQuestionAnswer::create([
							'quiz_question_id' => $question->id,
							'answer' => $answer,
							'correct' => ($key == 0) ? true : false
						]);
						//$quizanswer->save();
					}
				}
			}
		}

		// handle updated questions
		if ( ! is_null($request->get('question'))) {
			foreach ($request->get('question') as $question_key => $question) {
				$imageold = array();
				$image = array();
				$points = $request->get('points')[$question_key];
				$questionobj = QuizQuestion::where('id', $question_key)->first();
				$surFileName = $request->input('SurvivaltxtFileName')[$question_key];

				if( ! is_null($request->file('image'))) {
					if ( array_key_exists( $question_key, $request->file( 'image' ) ) ) {
						if ( ! is_null( $request->file( 'image' )[ $question_key ] ) ) {
							if($files = $request->file('image')[ $question_key ]){
								foreach($files as $file){
									$name = $file->getClientOriginalName();
									for ($i=0; $i < count($surFileName); $i++) {
			            				if($name == $surFileName[$i]){
								            $getFilename = $file->getFilename();
								            $file->move(public_path( 'storage/quiz/' . $Quiz->id ), $name );
								            $imageold[] = $name;
								        }
								    }
						        }
						    }
							/*$request->file( 'image' )[ $question_key ]->move( public_path( 'storage/quiz/' . $Quiz->id ), $request->file( 'image' )[ $question_key ]->getFilename() . '.' . $request->file( 'image' )[ $question_key ]->getClientOriginalExtension() );

							$questionobj->image = $request->file( 'image' )[ $question_key ]->getFilename() . '.' . $request->file( 'image' )[ $question_key ]->getClientOriginalExtension();*/
						}
					}
					foreach (json_decode($questionobj['image']) as $key) {
						for ($i=0; $i < count($surFileName); $i++) {
							if($key == $surFileName[$i]){
								$image[] = $key;
							}
						}
					}
					$imageold = array_merge($image, $imageold);
				}else{
					if(!empty($surFileName)){
						$imageold = $surFileName;
					}else{
						$imageold = array();
						$txtdescription = array();
					}
				}

				$questionobj->image = json_encode($imageold);
				$txtdescription = $request->get( 'txtdescription' )[ $question_key ];
				$questionobj->question_cat_id = $request->get('category')[ $question_key ];
				$questionobj->description = $request->get( 'questiondescription' )[ $question_key ];
				//$questionobj->imagedescription = (array_key_exists($question_key, $request->get('image_comment'))) ? $request->get('image_comment')[ $question_key ] : null;
				$questionobj->imagedescription = json_encode($txtdescription);
				$questionobj->question = $question;
				$questionobj->points = $points;
				$questionobj->save();

				foreach ($request->get('answer')[$questionobj->id] as $key => $answer) {
					if ($answer != '') {
						if (isset($request->get('correct_answer')[$questionobj->id])) {
							$correct_answer_val	= $request->get('correct_answer')[$questionobj->id];
							$correct_answer = ($correct_answer_val == $answer ) ? true : false;
						} else {
							$correct_answer = false;
						}

						$quizanswer = QuizQuestionAnswer::where('id', $key)->first();
						if ( ! is_null($quizanswer)) {
							$quizanswer->answer = $answer;
							$quizanswer->correct = $correct_answer;
							$quizanswer->save();
						} else {
							$new_answer = QuizQuestionAnswer::create([
								'quiz_question_id' => $questionobj->id,
								'answer' => $answer,
								'correct' => $request->get('correct_new_answer')[$questionobj->id] == 0 ? true : false,
							]);

							$answers = QuizQuestionAnswer::where('quiz_question_id', $questionobj->id)->get();
							foreach ($answers as $key => $item) {
								if ($new_answer->id != $item->id) {
									$item->correct = false;
									$item->save();
								}
							}
						}
					}
				}

				// save newly added answers
				if ( ! is_null( $request->get( 'newanswer' ) )  ) {
					if ( array_key_exists( $questionobj->id, $request->get( 'newanswer' ) ) ) {
						foreach ( $request->get( 'newanswer' )[ $questionobj->id ] as $key => $answer ) {
							if ( $answer != '' ) {
                      if(isset($request->get( 'correct_new_answer' )[ $questionobj->id ])){

								$correct_new_answer_key	=  $request->get( 'correct_new_answer' )[ $questionobj->id ];
								$new_correct_answer = 	($correct_new_answer_key == $key ) ? true : false;
							    } else {
								$new_correct_answer = 	false;
							    }
								// setting correct to false here because it is never if it did not exist on update.
								$quizanswer = QuizQuestionAnswer::create( [
									'quiz_question_id' => $questionobj->id,
									'answer'           => $answer,
									'correct'          => $new_correct_answer,
								] );
								$quizanswer->save();
							}
						}
					}
				}
			}
		}
		return redirect()->route( 'quiz.index' );
	}

	public function deleteQuestion($id) {
		$question = QuizQuestion::find($id);

		if($question) {
			$deleted = $question->delete();
			if($deleted) {
				return json_encode(['status' => 'OK']);
			}
			else {
				$response = [
					'status' => 'ERROR',
					'message' => 'Could not delete record'
					];
				return response()->json($response, 400);
			}
		}
		else {
			$response = [
				'status' => 'ERROR',
				'message' => 'Could not find record'
			];
			return response()->json($response, 400);
		}
	}

	public function downloadPDF($id)
	{
        $Quiz = Quiz::where( 'id', $id )->first();
        $questions = [];
        foreach($Quiz->QuizQuestion()->get()->load('QuestionAnswer') as $index => $question ) {
            $questions[QuizQuestionCategory::where('id', $question->question_cat_id)->first()->name][] = $question;
        }
		$pdf = \PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('pdf.invoice', compact('questions','Quiz'));
		return $pdf->download($Quiz->title.'.pdf');
	}

	public function deleteAnswer(Request $request, $id) {
		$answer = QuizQuestionAnswer::find($id);
		if($answer) {
			$deleted = $answer->delete();
			if($deleted) {
				return json_encode(['status' => 'OK']);
			}
			else {
				$response = [
					'status' => 'ERROR',
					'message' => 'Could not delete record'
				];
				return response()->json($response, 400);
			}
		}
		else {
			$response = [
				'status' => 'ERROR',
				'message' => 'Could not find record'
			];
			return response()->json($response, 400);
		}
	}

	public function submit(Request $request, $id ) {
		//User has passed (congratz)
		$submittedanswers = $request->all();
	    $user_id  = Auth::user()->id;
		unset($submittedanswers['_token']);
		$maxpoints = 0;
		foreach($submittedanswers as $questionid => $answerid) {
			$question = QuizQuestion::where( 'id', $questionid )->first();
			$maxpoints += $question->points;
			if($answerid == $question->CorrectQuestionAnswer()->id) {
				$checkedquestions[$questionid] = $question->points;
				$correct                       = 1;
				$correct_answer_id             = $question->CorrectQuestionAnswer()->id;
			} else {
				$checkedquestions[$questionid] = 0;
				$correct = 0;
				$correct_answer_id = $question->CorrectQuestionAnswer()->id;
			}
			$QuizResult= new QuizResult();
			$QuizResult->user_id             = $user_id;
			$QuizResult->quiz_id             = $id;
			$QuizResult->question_id         = $questionid;
			$QuizResult->answer_id           = $answerid;
			$QuizResult->correct_answer_id   = $correct_answer_id;
			$QuizResult->correct             = $correct;
			// add other fields
			$QuizResult->save();
		}
		$MailResults = QuizResult::where('user_id','=',$user_id)->where('quiz_id','=',$id)->get();
		$email  = Auth::user()->email;
		//$email  = 'devuthopian3@gmail.com';
		$awardedpoints = array_sum($checkedquestions);

		//Mail::send('emails.quiz.QuizResult',array('MailResults' => $MailResults), function($message) use ($email) {
            // $message->from(env('MAIL_USERNAME'), 'Need Secured');
             // $message->to($email);
           //});
		QuizSubmission::create( [
			'user_id' => Auth::user()->id,
			'quiz_id' => $id,
			'max_points' => $maxpoints,
			'awarded_points'  => $awardedpoints
		] );

	  // Auth::logout();

	 return redirect()->route('quiz.result', ['id' => $id]);
	}

	public function quizresult($id) {
	    $user_id  = Auth::user()->id;
		$correct = QuizResult::where('correct','=','1')->where('user_id','=',$user_id)->where('quiz_id','=',$id)->count();
		$total = QuizResult::where('user_id','=',$user_id)->where('quiz_id','=',$id)->count();
        $percentage = round((($correct / $total) * 100), 2);
		return view( 'quiz.quiz_result', [ 'quiz_id' => $id, 'percentage' => $percentage, 'total' => $total, 'correct' => $correct ] );
	}

	 public function downloadExcel($id) {
	 	$user_id  = Auth::user()->id;
	 	$data = QuizResult::where('user_id','=',$user_id)->where('quiz_id','=',$id)->get();
        return Excel::create('Quiz Result', function($excel) use ($data) {
            $excel->sheet('Quiz Res
            0ult', function($sheet) use ($data)
            {
                $sheet->cell('A1', function($cell) {$cell->setValue('Vraag');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('Jouw antwoord');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('Goed');   });
                $sheet->cell('D1', function($cell) {$cell->setValue('Goede antwoord');   });
                if (!empty($data)) {
                    foreach ($data as $key => $value) {
                    	if($value->quiz_answer->correct=='1'){
                           $correct = 'Ja';
                    	} else {
                    	   $correct = 'Nee';
                    	}
                        $i= $key+2;

                        $sheet->cell('A'.$i, $value->quiz_question->question);
                        $sheet->cell('B'.$i, $value->quiz_answer->answer);
                        $sheet->cell('C'.$i, $correct);
                        $sheet->cell('D'.$i, $value->correct_answer->answer);
                    }
                }
            });
        })->download('xlsx');
	}
}