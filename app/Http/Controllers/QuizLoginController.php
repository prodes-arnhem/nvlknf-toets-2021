<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use \App\QuizUser;
use \App\QuizSubmission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use DateTime;

class QuizLoginController extends Controller
{
	public function login() {
		return view( 'auth.quizlogin' );
	}

	/**
	 * Validate the user login request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return void
	 */
	public function handleLogin(Request $request) {
		$email = $request->input('email');
		$code = $request->input('password');
		$user = User::where('email', $email);

		// check if user exists
		if(!$user->exists()) {
			throw ValidationException::withMessages([
				$this->username() => [trans('auth.failed')],
			]);
		}

		$quizuser = $user->first()->QuizUsers()->where('code', $code);
		if(!$quizuser->exists()) {
			throw ValidationException::withMessages([
				'password' => [trans('auth.failed')],
			]);
		}

		if(new DateTime() > new DateTime($quizuser->first()->Quiz()->value('expires_at'))) {
			throw ValidationException::withMessages( [
				'password' => 'Toets is verlopen!',
			] );
		}
		$submission = QuizSubmission::where('user_id', $quizuser->value('user_id'))->where('quiz_id', $quizuser->value('quiz_id'));

		if(!$submission->get()->isEmpty()) {
			throw ValidationException::withMessages( [
				'password' => 'U heeft deze toets al een keer gemaakt',
			] );
		}

		$loggedin = Auth::loginUsingId($user->first()->id);

		if ( !$loggedin )
		{
			throw new Exception('Error logging in');
		}

		return redirect()->route('quiz.show', $quizuser->value('quiz_id'));
	}


	public function username()
	{
		return 'email';
	}
}
