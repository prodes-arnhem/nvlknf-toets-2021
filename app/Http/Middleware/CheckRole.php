<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
	    if(empty($request->user())) {
    		if($role == 'gebruiker') {
    			return redirect()->guest('/quiz/login');
		    }
		    else {
			    return redirect()->guest( 'login' );
		    }
   		}
   		elseif(!$request->user()->hasRole($role) && !$request->user()->hasRole('beheerder')) {
//	    	dd($request->user()->hasRole('beheerder'));
		    if($role == 'gebruiker') {
			    return redirect()->guest('/quiz/login');
		    }
		    else {
			    return redirect()->guest( 'login' );
		    }
	    }
	    else {
		    return $next($request);
	    }
    }
}
