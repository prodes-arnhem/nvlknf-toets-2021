<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


class Quiz extends Model
{


	protected $fillable = array('title', 'description','start','expires_at');

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'quizzes';

	public function scopeActive($query) {
		return $query->where('expires_at', '>=', Carbon::now()->format('Y-m-d H:i'))->orWhereNull('expires_at');
	}

	public function scopeInactive($query) {
		return $query->where('expires_at', '<', Carbon::now()->format('Y-m-d H:i'));
	}

	protected static function boot() {
		parent::boot();

		static::deleting(function($quiz) { // before delete() method call this
			$quiz->QuizQuestion()->each(function($question) {
				$question->delete();
			});
			$quiz->QuizUser()->each(function($quizuser) {
				$quizuser->delete();
			});
			$quiz->QuizSubmission()->each(function($submission) {
				$submission->delete();
			});
			// do the rest of the cleanup...
		});
	}


	public function QuizSubmission()
	{
		return $this->hasMany( \App\QuizSubmission::class );
	}

	public function QuizQuestion()
	{
		return $this->hasMany( \App\QuizQuestion::class );
	}

	public function QuizUser()
	{
		return $this->hasMany( \App\QuizUser::class );
	}

	public function getImageAttribute($value) {
		return url('storage/quiz/' . $this->id) . '/' . $value;
	}

}
