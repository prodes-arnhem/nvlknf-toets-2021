<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class QuizQuestion extends Model
{
	protected $fillable = array('quiz_id', 'question_cat_id', 'question', 'description', 'image', 'imagedescription', 'points');


	protected static function boot() {
		parent::boot();

		static::deleting(function($quizquestion) {
			// before delete() method call this
			$quizquestion->QuestionAnswer()->each(function($answer) {
				$answer->delete();
			});
			// do the rest of the cleanup...
		});
	}


	public function Quiz() {
		return $this->belongsTo( Quiz::class );
	}

	public function QuestionAnswer() {
		return $this->hasMany(QuizQuestionAnswer::class);
	}

	public function CorrectQuestionAnswer() {
		return $this->hasMany(QuizQuestionAnswer::class)->where('correct', '=', true)->first();

	}
}
