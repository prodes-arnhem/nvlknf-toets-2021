<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizQuestionAnswer extends Model
{
	protected $fillable = array('quiz_question_id', 'answer', 'correct');

	public function Question() {
		return $this->belongsTo( QuizQuestion::class );
	}
}
