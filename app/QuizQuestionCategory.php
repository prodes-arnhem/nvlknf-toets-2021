<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizQuestionCategory extends Model
{
	protected $fillable = array('id', 'name','cat_description','cat_image','cat_image_description');

}
