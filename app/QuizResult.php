<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\QuizQuestion;
use App\QuizQuestionAnswer;


class QuizResult extends Model
{
	protected $fillable = array('user_id', 'quiz_id', 'question_id','answer_id','correct_answer_id','correct');

	public function quiz_question()
	{
		return $this->belongsTo( QuizQuestion::class, 'question_id' );
	}

	public function quiz_answer()
	{
		return $this->belongsTo( QuizQuestionAnswer::class, 'answer_id' );
	}

	public function correct_answer()
	{
		return $this->belongsTo( QuizQuestionAnswer::class, 'correct_answer_id' );
	}
}
