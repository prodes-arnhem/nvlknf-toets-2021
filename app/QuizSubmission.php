<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Quiz;

class QuizSubmission extends Model
{

	protected $fillable = array('user_id', 'quiz_id', 'passed', 'max_points', 'awarded_points');

	public function user()
	{
		return $this->belongsTo( User::class );
	}

	public function Quiz()
	{
		return $this->belongsTo( Quiz::class );
	}
}
