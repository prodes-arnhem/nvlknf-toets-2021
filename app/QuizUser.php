<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizUser extends Model
{
	protected $fillable = array('user_id', 'quiz_id', 'code');

	public function user()
	{
		return $this->belongsTo( User::class );
	}

	public function Quiz()
	{
		return $this->belongsTo( Quiz::class );
	}
}
