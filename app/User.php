<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	public function QuizSubmissions()
	{
		return $this->hasMany(QuizSubmission::class);
	}

	public function QuizUsers() {
		return $this->hasMany(QuizUser::class);
	}

	public function hasRole($role) {
		return $role == $this->role;
	}

}
