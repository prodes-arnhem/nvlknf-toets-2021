<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageDescriptionToQuizQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('quiz_questions', function (Blueprint $table) {
		    $table->text('imagedescription', 10000)
		          ->after('image')
		          ->nullable();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('quiz_questions', function (Blueprint $table) {
		    $table->dropColumn('imagedescription');
	    });
    }
}
