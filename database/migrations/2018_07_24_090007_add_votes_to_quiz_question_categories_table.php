<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVotesToQuizQuestionCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quiz_question_categories', function (Blueprint $table) {
                $table->text('cat_description')->after('name')->nullable();
                $table->string('cat_image')->after('cat_description')->nullable();
                $table->string('cat_image_description')->after('cat_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quiz_question_categories', function (Blueprint $table) {
                        $table->dropColumn('cat_description');
                        $table->dropColumn('cat_image');
                        $table->dropColumn('cat_image_description');
        });
    }
}
