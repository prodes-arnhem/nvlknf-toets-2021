
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('moment');
require('eonasdan-bootstrap-datetimepicker');
require('moment-timezone');
window.tinymce = require('tinymce');
require('tinymce/plugins/link');
require('material-design-lite');
var MaterialStepper = require('mdl-stepper');
require('tinymce/themes/modern/theme');
window.PhotoSwipe = require('photoswipe/dist/photoswipe.js');
window.PhotoSwipeUI_Default = require('photoswipe/dist/photoswipe-ui-default.js');
// window.Vue = require('vue');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
//
// const app = new Vue({
//     el: '#app'
// });
