@extends('layouts.app')

@section('content')
    <div class="container c-quizform c-quizform--create">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Bewerk vraagcategorie</div>
                        <div class="panel-body">
                            <div class="row-fluid">
                                {!! Form::open(array('category.edit', 'POST', 'files' => true)) !!}
                                <div class="field-box">
                                    <label class="c-quizform__first-label">Naam</label><br />
                                    {!! Form::text('name', $category->name, array('required' => 'required')) !!} {!! $errors->first('name')!!}
                                </div>

                                <div class="field-box">
                                    <label>Omschrijving</label>
                                    <textarea rows="8" name="description" class="wysiwyg">{{ $category->cat_description }}</textarea>
                                </div>

                                <div class="questions">
                                    <div class="row">
                                        <h3 class="col-md-8">Afbeelding toevoegen</h3>
                                    </div>

                                    <div class="field-box question">
                                        <div class="row image">
                                            <label>Afbeelding</label>
                                            @php
                                                $cImages = json_decode($category->cat_image);
                                                $cimagedescription = json_decode($category->cat_image_description);
                                            @endphp
                                                @if(!is_null($cImages) && count($cImages) > 0)
                                                    {{-- <span id="result">
                                                        <img class="img-upload" src="/storage/category_images/{{$category->id . '/' . $cImages[0]}}">
                                                    </span> --}}
                                                    <?php for($im=0;$im<count($cImages);$im++) {?>
                                                        <div>
                                                            <input type="hidden" name="SurvivaltxtFileName[]" value="{{$cImages[$im]}}">
                                                            <img src="/storage/category_images/{{$category->id . '/' . $cImages[$im]}}" id="{{$cImages[$im]}}" class="imageThumb">
                                                            <label>Afbeeldings Omschrijving</label>
                                                            <input type="text" name="txtdescription[]" placeholder="Afbeeldings Omschrijving" value="{{$cimagedescription[$im]}}">
                                                            <span class="remove_image">X</span>
                                                        </div>
                                                    <?php }?>
                                                    <span class="question_1"></span>
                                                @else
                                                    <span id="result" class="question_1">
                                                        <img class="img-upload default-img" src="//placehold.it/600x400?text=Geen+afbeelding+gevonden">
                                                    </span>
                                                @endif
                                            <div class="input-group visible">
                                                <span class="input-group-btn">
                                                    <span class="btn btn-default btn-file">
                                                        <i class="fa fa-upload"><div class="picturetext"> Afbeelding uploaden</div></i>
                                                        <input type="file" name="image[]" class="imgInp" question="0"/>
                                                        {{-- <input type="file" name="image[]" class="imgInp" multiple="multiple" question="0"/> --}}
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />

                                <div class="pull-right">
                                    <a class="btn btn-danger" href="{{ route('category.index') }}">Annuleren</a>
                                    <button type="submit" class="btn btn-success btn-next" data-last="Finish">Opslaan</button>
                                </div>
                                {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        jQuery(function ($) {
            window.onbeforeunload = null;
            var answer = $('.answers').html();
            var questions = 0;
            addImageUploadHandlers();

            function addImageUploadHandlers() {
                // image upload
                $('.btn-file :file').unbind('change');
                $('.imgInp').unbind('change');
                $(document).on('change', '.btn-file :file', function (e) {
                    var input = $(this),
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', [label]);
                    $('.input-group.visible').removeClass('visible');
                    $('.row.image').append('<div class="input-group visible"><span class="input-group-btn"><span class="btn btn-default btn-file"><i class="fa fa-upload"><div class="picturetext"> Afbeelding uploaden</div></i><input type="file" name="image[]" class="imgInp lastInput" question="0"/></span></span></div>');
                    if (window.File && window.FileList && window.FileReader) {
                        var count = 0;
                        var files = e.target.files,
                            filesLength = files.length;

                        for (var i = 0; i < filesLength ; i++) {
                            var f = files[i];
                            console.log(f);
                            var fileReader = new FileReader();
                            fileReader.fileName = f.name;
                            fileReader.onload = (function(e) {
                                var file = e.target;
                                $('.question_1').append('<div><input type="hidden" name="SurvivaltxtFileName[]" value="'+e.target.fileName+'" /><img src="'+e.target.result+'" id="'+e.target.fileName+'" class="imageThumb"><input type="text" name="txtdescription[]" placeholder="Afbeeldingsomschrijving"><span class="remove_image">X</span></div>');
                            });
                            fileReader.readAsDataURL(f);
                        }
                        count++;

                    } else {
                        alert("Your browser doesn't support to File API")
                    }
                });

                $('.btn-file').on('fileselect', function (event, label) {
                    $('.img-upload').hide();
                    var input = $(this).find('.picturetext'),
                        log = 'Nog een afbeelding uploaded';
                        input.html(log);
                });

                /* if(window.File && window.FileList && window.FileReader) {
                    var count = 0;
                    $(".imgInp").on("change",function(e) {
                        var files = e.target.files,
                        filesLength = files.length;
                        for (var i = 0; i < filesLength ; i++) {
                            var f = files[i]
                            var fileReader = new FileReader();
                            fileReader.fileName = f.name
                            fileReader.onload = (function(e) {
                                var file = e.target;
                                $('.question_1').append('<div><input type="hidden" name="SurvivaltxtFileName[]" value="'+e.target.fileName+'"><img src="'+e.target.result+'" class="imageThumb" placeholder="Afbeeldings Omschrijving"><label>Afbeeldings Omschrijving</label><input name="txtdescription[]" type="text" class="form-control"><span class="remove_image">X</span></div>');
                            });
                            fileReader.readAsDataURL(f);
                        }
                        count++;
                    });
                } else {
                    alert("Your browser doesn't support to File API")
                } */
            }

            $('.imageThumb').click(function(event) {
                var getsrc = $(this).attr('src');
                $('.img-upload').attr('src', getsrc);
            });


        });

    </script>

    <script>
        $(function () {

            window.tinymce.init({
                selector: '.wysiwyg',
                plugins: "link",
                height: 300,
                content_style: '* {  font-family: "Raleway", sans-serif;\n' +
                '  font-size: 14px;\n' +
                '  line-height: 1.6;\n' +
                '  color: #636b6f;}'
            });

            // DateTimePicker
            $('.datepicker').datetimepicker({
                format: 'DD-MM-YYYY HH:mm'
            });

            // DateTimePicker
            $('.datetimepicker').datetimepicker({
                format: 'DD-MM-YYYY HH:mm'
            });


            // image upload
            $(document).on('change', '.btn-file :file', function () {
                var input = $(this),
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [label]);
            });

            $('.btn-file').on('fileselect', function (event, label) {
                $('.img-upload').hide();

                var input = $(this).find('.picturetext'),
                    log = 'Nog een afbeelding uploaded';

                input.html(log);

            });

            if(window.File && window.FileList && window.FileReader) {
                $(".imgInp").on("change",function(e) {
                    var files = e.target.files ,
                    filesLength = files.length;
                    var questionID = $(this).attr('question');
                    for (var i = 0; i < filesLength ; i++) {
                        var f = files[i]
                        var fileReader = new FileReader();
                        fileReader.onload = (function(e) {
                            var file = e.target;
                            $('.question_'+questionID).append('<div><img src="'+e.target.result+'" class="imageThumb"><span class="remove_image">X</span></div>');
                            /*$("<img></img>",{
                                class : "imageThumb",
                                src : e.target.result,
                                title : file.name
                            }).insertAfter("#imgInp");*/
                        });
                        fileReader.readAsDataURL(f);
                    }
                });
            } else {
                alert("Your browser doesn't support to File API")
            }
            $('.imageThumb').click(function(event) {
                var getsrc = $(this).attr('src');
                $('.img-upload').attr('src', getsrc);
            });

            /*function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    var img = $(input).parents('.image').find('img');

                    reader.onload = function (e) {
                        img.attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(".imgInp").change(function () {
                readURL(this);
            });*/
        });

        window.onbeforeunload = function () {
            return "Do you really want to leave our brilliant application?";
            //if we return nothing here (just calling return;) then there will be no pop-up question at all
            //return;
        };


        $(document).on('click', '.remove_image', function() {
            $(this).parent().remove();
           /* $(this).parent().find('img').remove();
            $(this).parent().find('span').remove();*/
        });
    </script>

@endsection