@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Vraag categorieën</div>


                    <div class="panel-body">
                        <div class="table-wrapper products-table">
                            <div class="row-fluid">
                                <div class="row">
                                    <div class="pull-right">
                                        <a class="btn btn-primary c-quizbutton" href="{{ route('quiz.index') }}">Quiz beheer</a>
                                        <a class="btn btn-primary c-quizbutton c-quizbutton--right" href="{{ route('category.create') }}">Nieuwe categorie</a>
                                    </div>
                                </div>
                                @if ($categories->count() > 0)

                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Naam</th>
                                            <th width="10%"></th>
                                            <th width="10%"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($categories as $category)
                                            <tr>
                                                <td>{{$category->name}}</td>
                                                <td class="buttoncell">
                                                    <a class="btn btn-warning" title="Bewerken" href="{{ route('category.edit', [ 'id' => $category->id ]) }}"><i class="fa fa-pencil"></i></a>
                                                </td>
                                                <td class="buttoncell"><a class="btn btn-danger" title="Verwijderen" href="{{ route('category.delete', [ 'id' => $category->id ]) }}"
                                                       onclick="return confirm('Weet je zeker dat je de categorie {{$category->name}} wilt verwijderen?')"><i class="fa fa-remove"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                @else
                                    <p>Er zijn geen categorieën gevonden</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection