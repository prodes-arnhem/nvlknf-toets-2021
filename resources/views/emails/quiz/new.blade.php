@component('mail::message')
Er staat een nieuwe toets voor u klaar, u kunt inloggen met de volgende gegevens:<br />
<br />
Code: {{ $user->code }}<br />
<br />
@component('mail::button', ['url' => config('app.url').'/quiz/login', 'color' => 'green'])
    Maak toets
@endcomponent
@endcomponent