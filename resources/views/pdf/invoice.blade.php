<!-- pdf Header -->
<div class="col-md-10 col-md-offset-1 pdf_header">
	<img  src="{{ public_path() }}/images/logo.png" width="10%" height="80%">
</div>

<div class="container" style="margin-top: 80px;">
	<hr>
    <div class="row page-break">
        <div class="col-md-10 col-md-offset-1">
          <div class="intro_step">
                       <div class="col-md-12">
                           <h3 style="text-align: center;">{{ $Quiz->title }}</h3>
                       </div>
                       <div class="col-md-12" style="text-align: center;">
                           {!! $Quiz->description !!}
                       </div>
              </div>  
              <br><hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                @foreach($questions as $category_name => $question_data)
                    <div class="page-break"></div>
                    <h3>{{ $category_name }}</h3>
            	    @foreach($question_data as $question)

            		    @php
                            $questimages = json_decode($question['image']);  @endphp
	            	    <div>
	            		    <b>
	            			    {{ $question['question'] }}
	            		    </b>
	            		    {!! $question['description'] !!}
	            		    @foreach($questimages as $questimage)
	            		    <img class="imageThumb" src="{{ public_path() }}/storage/quiz/{{ $quest['quiz_id'] }}/{{ $questimage }}">
	            		    @endforeach
	            	    </div>
	            	    <div>
                        <br>
                            @php
                                $az = range('A','Z');
                            @endphp

	            		    @foreach($question->QuestionAnswer as $index => $answers)
                                 @php
                                 @endphp
	            		        <i> {{ $az[$index] }}. {{ $answers->answer }}</i><br>

                            @endforeach
	            	    </div>
	            	    </br>
            	    @endforeach
                    <hr>
                @endforeach
			</div>
		</div>
	</div>
</div>
<div class="col-md-10 col-md-offset-1 pdf_footer">
	<img src="{{ public_path() }}/images/footer.png" width="100%" height="auto" class="footer_img">
</div>

<style type="text/css">
	.imageThumb {
		max-height: 75px;
		border: 2px solid;
		margin: 10px 10px 0 0;
		padding: 1px;
	}

.pdf_footer {
            position: fixed; 
            bottom: 0px; 
            left: 0px; 
            right: 0px;
            height: 150px; 
            /** Extra personal styles **/
            background-color: #fff;
            color: black;
            text-align: center;
            line-height: 35px;
            background-color: #fff;
            border-color: #d3e0e9;
        }
.page-break {
    page-break-after: always;
    page-break-inside: avoid;
        }

 .pdf_header {
            position: fixed;
            left: 0px;
            right: 0px;
            height: 70px;
            color: white;
            text-align: center;
            margin: :50px;
        }
       .footer_img{
       	position: fixed;
       	bottom: 37px;
       	left: 0;
       	width: 100%;
       		
        }
</style>