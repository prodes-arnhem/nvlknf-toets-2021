@extends('layouts.app')

@section('content')

    <style>
        .span3.answer:nth-child(4n+1) {
            margin-left: 0;
        }
    </style>
    <div class="container c-quizform c-quizform--create">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Quiz toevoegen</div>
                    <div class="panel-body">
                        {!! Form::open(array('quiz.create', 'POST', 'files' => true)) !!}

                        <div class="field-box">
                            <label class="c-quizform__first-label">Naam</label>
                            {!! Form::text('title', "", array('required' => 'required')) !!} {!! $errors->first('title')!!}
                        </div>

                        <div class="field-box">
                            <label>Start datum</label>
                            {!! Form::text('start', null, ['class' => 'datetimepicker']) !!} {!! $errors->first('title')!!}
                        </div>

                        <div class="field-box">
                            <label>Verloopdatum</label>
                            {!! Form::text('expires_at', null, ['class' => 'datetimepicker']) !!} {!! $errors->first('title')!!}
                        </div>

                        <div class="field-box">
                            <label>Omschrijving</label>
                            <textarea rows="8" name="description" class="wysiwyg"></textarea>
                            {{ Form::select('category[0]', $categories, null, ['required', 'class' => 'form-control']) }}
                        </div>


                        <div class="questions">
                            <div class="row">
                                <h3 class="col-md-8">Vragen</h3>
                            </div>
                            <div class="field-box question">
                                <label>Categorie</label>
                                {{ Form::select('category[0]', $categories, null, ['required', 'class' => 'form-control']) }}
                                <label>Omschrijving</label>
                                <textarea rows="8" name="questiondescription[0]" class="wysiwyg"></textarea>
                                <div class="row image">
                                    <label>Afbeelding</label>
                                    <span id="result" class="question_0">
                                        <img class="img-upload" src="//placehold.it/600x400?text=Geen+afbeelding+gevonden">
                                    </span>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file">
                                                <i class="fa fa-upload"><div class="picturetext"> Afbeelding uploaden</div></i>
                                                <input type="file" name="image[0][]" id="imgInp" class="imgInp" multiple="multiple" question="0"/>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                                <label>Vraag</label>
                                <input name="question[0]" type="text" required>
                                <label>Aantal punten</label>
                                <input name="points[0]" type="number" value="1" required>
                                <div style=" position: relative;">
                                    <div class="row answers">
                                    <div class="span3 answer" style="margin-left:-32px;">
                                     <div class="col-md-5">
                                        <label>antwoord</label>
                                        <input name="answer[0][]" type="text" required>
                                       </div>
                                         <div class="col-md-4" style="margin-top: 42px;">
                                                <strong>Juist antwoord</strong>
                                                <input type="checkbox" name="correct_answer[0]" value="0"  >
                                        </div>

                                    </div>
                                </div>
                                <a href="#" class="add-answer" style="position: absolute; right: 0px; bottom:20px;" data-id="0"><i class="fa fa-plus"></i> Additioneel antwoord toevoegen</a>    
                                </div>
                                
                            </div>

                        </div>

                        <div class="row pull-right">
                            <button type="button" class="add-question btn btn-default"><i class="fa fa-plus"></i> Vraag toevoegen</button>
                            <a href="{{ route('quiz.index') }}" class="btn btn-danger btn-next" data-last="Finish">Annuleren</a>
                            <button type="submit" class="btn btn-success btn-next c-quizbutton--right" data-last="Finish">Opslaan
                            </button>
                        </div>

                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>

        jQuery(function ($) {
             window.onbeforeunload = null;
            var answer = $('.answers').html();
            var questions = 0;
            var categories = <?= json_encode( $categories ); ?>;
//            console.log(categories);

            $('.add-question').click(function (e) {
                e.preventDefault();
                questions += 1;
                var categorie_selector = '<select class="form-control" required name="category[' + questions + ']">';
                categorie_selector += '<option>Selecteer een categorie...</option>';
                $.each(categories, function (index, element) {
                    categorie_selector += '<option value="' + index + '">' + element + '</option>';
                });
                categorie_selector += '</select>';

//                    console.log(categorie_selector);

                $('.questions').append('<div class="field-box question">\
                    <label>Vraag <a href="#" class="pull-right delete-question">Vraag verwijderen</a></label>\
                    <label>Categorie</label>\
                    ' + categorie_selector + '\
                    <label>Omschrijving</label>\
                    <textarea rows="8" name="questiondescription[' + questions + ']" class="wysiwyg"></textarea>\
                     <div class="row image">\
                    <label>Plaatje</label>\
                    <span id="result" class="question_'+ questions +'">\
                        <img class="img-upload" src="//placehold.it/600x400?text=Geen+afbeelding+gevonden">\
                    </span>\
                    <div class="input-group">\
                    <span class="input-group-btn">\
                 <span class="btn btn-default btn-file">\
                    <i class="fa fa-upload"><div class="picturetext pull-right"> Afbeelding uploaden</div></i>\
                    <input type="file" name="image[' + questions + '][]"  id="imgInp" class="imgInp" multiple="multiple" question="'+ questions + '"/>\
                        </span>\
                </span>\
                </div>\
                </div>\
                    <input name="question[' + questions + ']" type="text">\
                    <label>Aantal punten</label>\
                    <input required name="points[' + questions + ']" value="1" type="number">\
                    <div style="position:relative;">\
                    <div class="row answers">\
                       \
                         <div class="span3 answer" style="margin-left:-32px;">\
                                     <div class="col-md-5">\
                                        <label>antwoord</label>\
                                        <input name="answer[' + questions + '][]" type="text" required>\
                                    </div>\
                                         <div class="col-md-4" style="margin-top: 42px;">\
                                                <strong>Juist antwoord</strong>\
                                                <input type="checkbox" name="correct_answer[' + questions + ']" value="0"  >\
                                            </div>\
\
                                    </div>\
                    </div>\
                    <a href="#" style="position:absolute; right:0; bottom:20px;"class="add-answer" data-id="' + questions + '"><i class="fa fa-plus"></i> Additioneel antwoord toevoegen</a>\
                </div>\
                </div>\
                </div>');

                $('.add-answer').unbind('click');
                addClickHandlerAnswer($('.add-answer'));
                addClickHandlerRemoveQuestion($('.delete-question'));
                addImageUploadHandlers();
                window.tinymce.init({
                    selector: '.wysiwyg',
                    content_style: '* {  font-family: "Raleway", sans-serif;\n' +
                    '  font-size: 14px;\n' +
                    '  line-height: 1.6;\n' +
                    '  color: #636b6f;}'
                });

                var body = $("html, body");
                body.stop().animate({scrollTop: $('.questions').prop('scrollHeight') - 500}, 500, 'swing', function () {
                });

            });

            addClickHandlerAnswer($('.add-answer'));
            addClickHandlerRemoveQuestion($('.delete-question'));
            addClickHandlerRemoveAnswer($('.delete-answer'));

            function addClickHandlerAnswer(element) {
                     var rowNum = 1;
                element.click(function (e) {
                    e.preventDefault();

                    $(this).parent().find('.answers').append('<div class="span3 answer" style="margin-left:-32px;">\
                        \
                        <label style="margin-left: 16px; width:60%;">Additioneel antwoord <a href="#" class="pull-right delete-answer"><i class="fa fa-remove"></i> Verwijderen</a></label>\
                        <div class="col-md-5">\
                        <input name="answer[' + $(this).attr('data-id') + '][]" type="text" required>\
                         \
                         </div>\
                         <div class="col-md-4">\
                                                <strong>Juist antwoord</strong>\
                                                <input type="checkbox" name="correct_answer[' + $(this).attr('data-id') + ']" value ="'+ rowNum +'"  >\
                                            </div>\
                    </div>');

                    addClickHandlerRemoveAnswer($('.delete-answer'));
                    rowNum++;
                });
            }

            function addClickHandlerRemoveQuestion(element) {
                element.click(function (e) {
                    e.preventDefault();

                    $(this).parent().parent().remove();
                });
            }

            function addClickHandlerRemoveAnswer(element) {
                element.click(function (e) {
                    e.preventDefault();

                    $(this).parent().parent().remove();
                });
            }

            function addImageUploadHandlers() {
                // image upload
                $('.btn-file :file').unbind('change');
                $('.imgInp').unbind('change');
                $(document).on('change', '.btn-file :file', function () {
                    var input = $(this),
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', [label]);
                });

                $('.btn-file').on('fileselect', function (event, label) {
                   $('.img-upload').hide();
                    var input = $(this).find('.picturetext'),
                        log = 'Nog een afbeelding uploaded';

                    input.html(log);

                });

                /*function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        var img = $(input).parents('.image').find('span');

                        reader.onload = function (e) {
                            imga.append('<img src="'+e.target.result+'" class="img-upload">')
                            img.attr('src', e.target.result);
                        };

                        reader.readAsDataURL(input.files[0]);
                    }
                }

                $(".imgInp").change(function () {
                    readURL(this);
                });*/
                if(window.File && window.FileList && window.FileReader) {
                    $(".imgInp").on("change",function(e) {
                        var files = e.target.files ,
                        filesLength = files.length;
                        var questionID = $(this).attr('question');
                        for (var i = 0; i < filesLength ; i++) {
                            var f = files[i]
                            var fileReader = new FileReader();
                            fileReader.fileName = f.name
                            fileReader.onload = (function(e) {
                                var file = e.target;
                                $('.question_'+questionID).append('<div><input type="hidden" name="SurvivaltxtFileName['+questionID+'][]" value="'+e.target.fileName+'"><img src="'+e.target.result+'" id="'+e.target.fileName+'" class="imageThumb"><input type="text" name="txtdescription['+questionID+'][]" placeholder="Afbeeldingsomschrijving"><span class="remove_image">X</span></div>');
                            });
                            fileReader.readAsDataURL(f);
                        }
                    });
                } else {
                    alert("Your browser doesn't support to File API") 
                }
            }

            $('.imageThumb').click(function(event) {
                var getsrc = $(this).attr('src');
                $('.img-upload').attr('src', getsrc);
            });
            

        });

    </script>
    <script>
        $(function () {

            window.tinymce.init({
                selector: '.wysiwyg',
                plugins: "link",
                height: 300,
                content_style: '* {  font-family: "Raleway", sans-serif;\n' +
                '  font-size: 14px;\n' +
                '  line-height: 1.6;\n' +
                '  color: #636b6f;}'
            });

            // DateTimePicker
            $('.datepicker').datetimepicker({
                format: 'DD-MM-YYYY HH:mm'
            });

            // DateTimePicker
            $('.datetimepicker').datetimepicker({
                format: 'DD-MM-YYYY HH:mm'
            });


            // image upload
            $(document).on('change', '.btn-file :file', function () {
                var input = $(this),
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [label]);
            });

            $('.btn-file').on('fileselect', function (event, label) {
                $('.img-upload').hide();

                var input = $(this).find('.picturetext'),
                    log = 'Nog een afbeelding uploaded';

                input.html(log);

            });

            if(window.File && window.FileList && window.FileReader) {
                $(".imgInp").on("change",function(e) {
                    var files = e.target.files ,
                    filesLength = files.length;
                    var questionID = $(this).attr('question');
                    for (var i = 0; i < filesLength ; i++) {
                        var f = files[i]
                        var fileReader = new FileReader();
                        fileReader.fileName = f.name
                        fileReader.onload = (function(e) {
                            var file = e.target;
                            $('.question_'+questionID).append('<div><input type="hidden" name="SurvivaltxtFileName['+questionID+'][]" value="'+e.target.fileName+'"><img src="'+e.target.result+'" id="'+e.target.fileName+'" class="imageThumb"><input type="text" name="txtdescription['+questionID+'][]" placeholder="Afbeeldingsomschrijving"><span class="remove_image">X</span></div>');
                            /*$("<img></img>",{
                                class : "imageThumb",
                                src : e.target.result,
                                title : file.name
                            }).insertAfter("#imgInp");*/
                        });
                        fileReader.readAsDataURL(f);
                    }
                });
            } else {
                alert("Your browser doesn't support to File API") 
            }
            $('.imageThumb').click(function(event) {
                var getsrc = $(this).attr('src');
                $('.img-upload').attr('src', getsrc);
            });

            /*function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    var img = $(input).parents('.image').find('img');

                    reader.onload = function (e) {
                        img.attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(".imgInp").change(function () {
                readURL(this);
            });*/
        });

        window.onbeforeunload = function () {
            return "Do you really want to leave our brilliant application?";
            //if we return nothing here (just calling return;) then there will be no pop-up question at all
            //return;
        };


       $(document).on('click', '.remove_image', function() { 
        //$(this).parent().find('img').remove();
        //$(this).parent().find('span').remove();.
        $(this).parent().remove();
        });
    </script>

@stop