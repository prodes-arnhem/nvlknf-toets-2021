@extends('layouts.app')

@section('content')
    <style>
        .span3.answer:nth-child(4n+1) {
            margin-left: 0;
        }
    </style>
    <div class="container c-quizform">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Toets bewerken</div>

                    <div class="panel-body">
                        <div class="row pull-right">
                            <a href="{{ route('quiz.index') }}" class="btn btn-default c-quizbutton c-quizbutton--right">Terug</a>
                        </div>
                        {!! Form::open(array('toolbox/save', 'POST', 'files' => true)) !!}

                        <div class="field-box">
                            <label class="c-quizform__first-label">Naam</label>
                            {!! Form::text('title', $Quiz->title) !!}
                        </div>

                        <div class="field-box">
                            <label>Start datum</label>
                            {!! Form::text('start', empty($Quiz->start) ? null : \Carbon\Carbon::parse($Quiz->start), ['class' => 'datetimepicker']) !!} {!! $errors->first('title')!!}
                        </div>

                        <div class="field-box">
                            <label>Verloopdatum</label>
                            {!! Form::text('expires_at', empty($Quiz->expires_at) ? null : \Carbon\Carbon::parse($Quiz->expires_at), ['class' => 'datetimepicker']) !!} {!! $errors->first('title')!!}
                        </div>

                        <div class="field-box">
                            <label>Omschrijving</label>
                            <textarea rows="8" name="description" class="wysiwyg">{{$Quiz->description}}</textarea>
                        </div>

						<?php $i = 0; ?>
                        <div class="questions">
                            <div class="row">
                                <h3 class="col-md-8">Vragen</h3>
                            </div>
                            @foreach($Quiz->QuizQuestion()->get() as $question)
                                <div class="field-box question">
                                    <label>Categorie</label>
                                    {{ Form::select('category['.$question->id.']', $categories, $question->question_cat_id, ['required', 'class' => 'form-control']) }}
                                    <label>Omschrijving</label>
                                    <textarea rows="8" name="questiondescription[{{$question->id}}]" class="wysiwyg">{{$question->description}}</textarea>
                                    <div class="row image">
                                        <label>Afbeelding</label>
                                        @php
                                            $cImages = json_decode($question->image);
                                            $cimagedescription = json_decode($question->imagedescription);
                                        @endphp
                                        @if(!is_null($cImages) && count($cImages) > 0)
                                            <span id="result">
                                                <img class="img-upload" src="/storage/quiz/{{$Quiz->id}}/{{$cImages[0]}}">
                                            </span>
                                            <?php for($im=0;$im<count($cImages);$im++) {?>
                                                <div>
                                                    <input type="hidden" name="SurvivaltxtFileName[{{$question->id}}][]" value="{{$cImages[$im]}}">
                                                    <img src="/storage/quiz/{{$Quiz->id}}/{{$cImages[$im]}}" id="{{$cImages[$im]}}" class="imageThumb">
                                                    <label>Afbeeldingsomschrijving</label>
                                                    <input type="text" name="txtdescription[{{$question->id}}][]" placeholder="Afbeeldingsomschrijving" value="{{$cimagedescription[$im]}}">
                                                    <span class="remove_image">X</span>
                                               </div>
                                            <?php }?>
                                            <span class="question_{{$question->id}}"></span>

                                        @else
                                            <span id="result" class="question_{{$question->id}}">
                                                <img class="img-upload default-img" src="//placehold.it/600x400?text=Geen+afbeelding+gevonden">
                                            </span>
                                        @endif
                                        <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file">
                                                <i class="fa fa-upload"><div class="picturetext"> Afbeelding uploaden</div></i>
                                                <input type="file" name="image[{{$question->id}}][]" id="imgInp" class="imgInp" multiple="multiple" question="{{$question->id}}"/>
                                            </span>
                                        </span>
                                        </div>
                                    </div>
                                    <label>Vraag <a href="#" data-id="{{$question->id}}" class="pull-right delete-existing-question">Vraag verwijderen</a></label>
                                    <input name="question[{{$question->id}}]" value="{{$question->question}}" type="text" required>
                                    <label>Aantal punten</label>
                                    <input name="points[{{$question->id}}]" value="{{$question->points}}" type="number" required>
                                    <div style=" position: relative; display: inline-block;width: 100%;">
                                        <div class="answers">
                                            <?php $answers = 0; ?>
                                            @foreach ($question->QuestionAnswer()->get() as $answer)
                                                <div class="span3 answer" style="margin-left:-32px;">
                                                    <label style="margin-left: 16px;width: 52%;">Additioneel antwoord <a href="#" data-id="{{$answer->id}}" class="pull-right delete-existing-answer"><i class="fa fa-remove"></i> Verwijderen</a></label>
                                                    <div class="col-md-7">
                                                        <input name="answer[{{$question->id}}][{{$answer->id}}]" value="{{$answer->answer}}" type="text" required>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <strong>Juist antwoord</strong>
                                                        <input name="correct_answer[{{$question->id}}]" value="{{$answer->answer}}" {{ $answer->correct == '1' ? 'checked' : '' }} type="checkbox" >
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>
                                                <?php $answers++; ?>
                                            @endforeach
                                        </div>

                                        <a href="#" style="position: absolute; right: 0px; bottom:0px;" class="add-answer" data-id="{{$question->id}}"><i class="fa fa-plus"></i> Additioneel antwoord toevoegen</a>
                                    </div>

                                    <!-- <div class="row imagedescription">
                                        <label>Afbeeldingsomschrijving</label>
                                        <input name="image_comment[{{$question->id}}]" type="text" value="{{ $question->imagedescription }}" class="form-control">
                                    </div> -->
                                </div>
								<?php $i++; ?>
                            @endforeach
                        </div>

                        <div class="row pull-right">
                            <button type="button" class="add-question btn btn-default"><i class="fa fa-plus"></i> Vraag toevoegen</button>
                            <a href="{{ route('quiz.delete', [ 'id' => $Quiz->id ]) }}" class="btn btn-danger btn-next" data-last="Finish">Verwijderen</a>
                            <button type="submit" class="btn btn-success btn-next c-quizbutton--right" data-last="Finish">Opslaan</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        jQuery(function($) {
            window.onbeforeunload = null;
            var answer = $('.answers').html();
            var questions = 0;
            var categories = <?= json_encode($categories); ?>;
            //console.log(questions);

            $('.add-question').click(function(e) {
                e.preventDefault();

                var categorie_selector ='<select class="form-control" required name="newquestioncategory[' + questions + ']">';
                categorie_selector += '<option>Selecteer een categorie...</option>';
                $.each(categories, function( index, element ) {
                    categorie_selector += '<option value="'+ index +'">' + element + '</option>';
                });
                categorie_selector += '</select>';

                $('.questions').append('<div class="field-box question">\
                    <label> <a href="#" class="pull-right delete-question">Vraag verwijderen</a></label>\
                     <label>Categorie</label>\
                    '+ categorie_selector +'\
                    <label>Omschrijving</label>\
                    <textarea rows="8" name="newquestiondescription['+ questions +']" class="wysiwyg"></textarea>\
                    <label>Vraag</label>\
                    <input name="newquestion[' + questions + ']" type="text" required>\
                    <label>Aantal punten</label>\
                    <input name="newpoints[' + questions +']" type="number" required>\
                    <div class="row image">\
                    <label>Plaatje</label>\
                    <span id="result" class="question_'+ questions +'">\
                        <img class="img-upload" src="//placehold.it/600x400?text=Geen+afbeelding+gevonden">\
                    </span>\
                    <div class="input-group">\
                    <span class="input-group-btn">\
                      <span class="btn btn-default btn-file">\
                    <i class="fa fa-upload"><div class="picturetext pull-right"> Afbeelding uploaden</div></i>\
                    <input type="file" name="newimage[' + questions + '][]"  id="imgInp" class="imgInp" multiple="multiple" question="'+ questions + '"/>\
                        </span>\
                </span>\
                    </span>\
                </div>\
                    <div style="position:relative;">\
                    <div class="row answers">\
                       \
                         <div class="span3 answer" style="margin-left:-32px;">\
                                     <div class="col-md-5">\
                                        <label>antwoord</label>\
                                        <input name="answer[' + questions + '][]" type="text" required>\
                                    </div>\
                                         <div class="col-md-4" style="margin-top: 42px;">\
                                                <strong>Juist antwoord</strong>\
                                                <input type="checkbox" name="correct_answer[' + questions + ']" value="0"  >\
                                            </div>\
\
                                    </div>\
                                    <div class="clearfix"></div>\
                    </div>\
                    <a href="#" style="position:absolute; right:0; bottom:20px;"class="add-answer" data-id="' + questions + '"><i class="fa fa-plus"></i> Additioneel antwoord toevoegen</a>\
                </div>\
                </div>\
                </div>');

                questions += 1;

                $('.add-answer').unbind('click');
                addClickHandlerAnswer($('.add-answer'));
                addClickHandlerRemoveQuestion($('.delete-question'));
                addImageUploadHandlers();
                window.tinymce.init({ selector:'.wysiwyg'});
                var body = $("html, body");
                body.stop().animate({scrollTop:$('.questions').prop('scrollHeight')-500}, 500, 'swing', function() {
                });
            });

            addClickHandlerAnswer($('.add-answer'));
//            addClickHandlerRemoveQuestion($('.delete-question'));
//            addClickHandlerRemoveAnswer($('.delete-answer'));
            addAjaxHandlerRemoveQuestion($('.delete-existing-question'));
            addAjaxHandlerRemoveAnswer($('.delete-existing-answer'));
            addImageUploadHandlers();

            function addClickHandlerAnswer(element,newanswer) {
                var rowNum = 0;
                element.click(function(e) {
                    e.preventDefault();
                    $(this).parent().find('.answers').append('<div class="span3 answer" style="margin-left:-32px;">\
                        \
                        <label style="margin-left: 16px;width: 52%;">Additioneel antwoord <a href="#" class="pull-right delete-answer"><i class="fa fa-remove"></i> Verwijderen</a></label>\
                        <div class="col-md-7">\
                        <input name="answer[' + $(this).attr('data-id') + '][]" type="text" required>\
                         \
                         </div>\
                         <div class="col-md-4">\
                                                <strong>Juist antwoord</strong>\
                                                <input type="checkbox" name="correct_new_answer[' + $(this).attr('data-id') + ']" value ="'+ rowNum +'"  >\
                                            </div><div class="clearfix"></div>\
                    </div>');
                    addClickHandlerRemoveAnswer($('.delete-answer'));
                    rowNum++;
                });
            }

            function addClickHandlerRemoveQuestion(element) {
                element.click(function(e) {
                    e.preventDefault();

                    $(this).parent().parent().remove();
                });
            }
            function addAjaxHandlerRemoveAnswer(element) {
                element.click(function(e) {
                    e.preventDefault();
                    if (window.confirm("Are you sure? This cannot be undone!")) {

                        var parentdiv = $(this).parent().parent();
                        var request = $.ajax({
                            url: "/answer/delete/" + $(this).attr('data-id'),
                            type: "post",
                            success: function (data) {
                                parentdiv.remove();
                            }
                        });
                    }
                });
            }
            function addAjaxHandlerRemoveQuestion(element) {
                element.click(function(e) {
                    e.preventDefault();
                    if (window.confirm("Are you sure? This cannot be undone!")) {

                        var parentdiv = $(this).parent().parent();
                        var request = $.ajax({
                            url: "/question/delete/" + $(this).attr('data-id'),
                            type: "post",
                            success: function (data) {
                                parentdiv.remove();
                            }
                        });
                    }
                });
            }

            function addClickHandlerRemoveAnswer(element) {
                element.click(function(e) {
                    e.preventDefault();

                    $(this).parent().parent().remove();
                });
            }

            function addImageUploadHandlers() {
                // image upload
                $('.btn-file :file').unbind('change');
                $('.imgInp').unbind('change');
                $(document).on('change', '.btn-file :file', function () {
                    var input = $(this),
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', [label]);
                });

                $('.btn-file').on('fileselect', function (event, label) {
                     $('.default-img').hide();
                     $('.img-upload').hide();
                    var input = $(this).find('.picturetext'),
                    log = 'Nog een afbeelding uploaded';
                    input.html(log);

                });

                /*function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        var img = $(input).parents('.image').find('img');

                        reader.onload = function (e) {
                            img.attr('src', e.target.result);
                        };

                        reader.readAsDataURL(input.files[0]);
                    }
                }

                $(".imgInp").change(function () {
                    readURL(this);
                });*/

                if(window.File && window.FileList && window.FileReader) {
                    $(".imgInp").on("change",function(e) {
                        var files = e.target.files ,
                        filesLength = files.length;
                        var questionID = $(this).attr('question');
                        for (var i = 0; i < filesLength ; i++) {
                            var f = files[i]
                            var fileReader = new FileReader();
                            fileReader.fileName = f.name
                            fileReader.onload = (function(e) {
                                var file = e.target;
                                $('.question_'+questionID).append('<div><input type="hidden" name="SurvivaltxtFileName['+questionID+'][]" value="'+e.target.fileName+'"><img src="'+e.target.result+'" id="'+e.target.fileName+'" class="imageThumb"><input type="text" name="txtdescription['+questionID+'][]" placeholder="Afbeeldingsomschrijving"><span class="remove_image">X</span></div>');
                                //$('.question_'+questionID).append('<div><img src="'+e.target.result+'" class="imageThumb"><span class="remove_image">X</span></div>');
                            });
                            fileReader.readAsDataURL(f);
                        }
                    });
                } else {
                    alert("Your browser doesn't support to File API")
                }
            }


        });

    </script>
    <script>

        $(function() {
            window.tinymce.init({
                selector: '.wysiwyg',
                height: 300,
                content_style: '* {  font-family: "Raleway", sans-serif;\n' +
                '  font-size: 14px;\n' +
                '  line-height: 1.6;\n' +
                '  color: #636b6f;}'
            });


            // DateTimePicker
            $('.datepicker').datetimepicker({
                format: 'DD-MM-YYYY HH:mm'
            });

            // DateTimePicker
            $('.datetimepicker').datetimepicker({
                format: 'DD-MM-YYYY HH:mm'
            });

            $('.imageThumb').click(function(event) {
                var getsrc = $(this).attr('src');
                $('.img-upload').attr('src', getsrc);
            });

        });

        // Enable navigation prompt
        window.onbeforeunload = function() {
            return "Do you really want to leave our brilliant application?";
            //if we return nothing here (just calling return;) then there will be no pop-up question at all
            //return;
        };

       $(document).on('click', '.remove_image', function() {
       $(this).parent().remove();
       /* $(this).parent().find('img').remove();
        $(this).parent().find('span').remove();*/
        });


    </script>
@stop