@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Toetsen</div>


                    <div class="panel-body">
                        <div class="table-wrapper products-table">
                            <div class="row-fluid">
                                <div class="row">
                                <h4 class="col-md-6">Actieve toetsen</h4>

                                <div class="pull-right">
                                    <a class="btn btn-primary c-quizbutton" href="{{ route('category.index') }}">Vraagcategorie beheer</a>
                                    <a class="btn btn-primary c-quizbutton c-quizbutton--right" href="{{ route('quiz.create') }}">Nieuwe Toets</a>
                                </div>
                                </div>

                                @if ($activeQuizzes->count() > 0)

                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Titel</th>
                                            <th width="15%">Start op</th>
                                            <th width="15%">Verloopt op</th>
                                            <th width="10%"></th>
                                            <th width="10%"></th>
                                            <th width="10%"></th>
                                            <th width="10%"></th>
                                            <th width="10%"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($activeQuizzes as $Quiz)
                                            <tr>
                                                <td>{{$Quiz->title}}</td>
                                                <td>{{ $Quiz->start or "--" }}</td>
                                                <td>{{ $Quiz->expires_at or "--" }}</td>
                                                <td class="buttoncell">
                                                    <a class="btn btn-primary" title="Bekijken" href="{{ route('quiz.show', [ 'id' => $Quiz->id ]) }}"><i class="fa fa-eye"></i></a>
                                                </td>
                                                <td class="buttoncell">
                                                    <a class="btn btn-warning" title="Bewerken" href="{{ route('quiz.edit', [ 'id' => $Quiz->id ]) }}"><i class="fa fa-pencil"></i></a>
                                                </td>
                                                <td class="buttoncell"><a class="btn btn-success" title="Resultaten" href="{{ route('quiz.results', [ 'id' => $Quiz->id ]) }}"><i class="fa fa-bar-chart"></i></a>
                                                </td>
                                                <td class="buttoncell"><a class="btn btn-info" title="{{$Quiz->title}}" href="{{url('/question/pdf')}}/{{$Quiz->id}}"><i class="fa fa-file-pdf-o"></i></a>
                                                </td>
                                                <td class="buttoncell"><a class="btn btn-danger" title="Verwijderen" href="{{ route('quiz.delete', [ 'id' => $Quiz->id ]) }}"
                                                       onclick="return confirm('Weet je zeker dat je de toets {{$Quiz->title}} wilt verwijderen?')"><i class="fa fa-remove"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                @else
                                    <p>Er zijn geen actieve toetsen gevonden</p>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="table-wrapper products-table">
                            <div class="row-fluid">

                                <h4>Verlopen toetsen</h4>

                                @if ($inactiveQuizzes->count() > 0)

                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Titel</th>
                                            <th width="15%">Start op</th>
                                            <th width="15%">Verlopen op</th>
                                            <th width="10%"></th>
                                            <th width="10%"></th>
                                            <th width="10%"></th>
                                            <th width="10%"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($inactiveQuizzes as $Quiz)
                                            <tr>
                                                <td >{{$Quiz->title}}</td>
                                                <td >{{\Carbon\Carbon::parse($Quiz->start)->format('d-m-Y H:i')}}</td>
                                                <td>{{\Carbon\Carbon::parse($Quiz->expires_at)->format('d-m-Y H:i')}}</td>
                                                <td class="buttoncell">
                                                    <a class="btn btn-primary" title="Bekijken" href="{{ route('quiz.show', [ 'id' => $Quiz->id ]) }}"><i class="fa fa-eye"></i></a>
                                                </td>
                                                <td class="buttoncell">
                                                    <a class="btn btn-warning" title="Bewerken" href="{{ route('quiz.edit', [ 'id' => $Quiz->id ]) }}"><i class="fa fa-pencil"></i></a>
                                                </td>
                                                <td class="buttoncell"><a class="btn btn-success" title="Resultaten" href="{{ route('quiz.results', [ 'id' => $Quiz->id ]) }}"><i class="fa fa-bar-chart"></i></a>
                                                </td>
                                                <td class="buttoncell"><a class="btn btn-info" title="{{$Quiz->title}}" href="{{url('/question/pdf')}}/{{$Quiz->id}}"><i class="fa fa-file-pdf-o"></i></a>
                                                </td>
                                                <td class="buttoncell"><a class="btn btn-danger" title="Verwijderen" href="{{ route('quiz.delete', [ 'id' => $Quiz->id ]) }}"
                                                       onclick="return confirm('Weet je zeker dat je de toets {{$Quiz->title}} wilt verwijderen?')"><i class="fa fa-remove"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                @else
                                    <p>Er zijn geen verlopen toetsen gevonden</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection