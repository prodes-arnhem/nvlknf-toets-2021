@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span><a href="{{url('/quiz/excel')}}/{{ $quiz_id }}">Download Excel</a></span><br>
                        Quiz resultaten
                    </div>
                    <div class="panel-body">
                        <h3>Standaard voor succes> Aantal per testpercentage geeft 0-60% en 61-100% aan </h3>
                        <h4> {{$correct}} of {{$total}}</h4> <br>
                        <h4> {{ $percentage }} % </h4>
                        @if($percentage >= 61)
                            <h3>je bent geslaagd</h3>
                        @else
                            <h3> je bent niet geslaagd</h3>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection