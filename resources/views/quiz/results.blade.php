@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Quiz resultaten - {{ $quiz->title }}</div>

                    <div class="panel-body">
                        @if(Session::has('message'))
                            <span class="message">{!! Session::get('message') !!}</span>
                        @endif
                            <div class="row pull-right">
                                <a href="{{ route('quiz.index') }}" class="btn btn-default c-quizbutton--right">Terug</a>
                            </div>
                        @if ($users->count() > 0)

                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Naam</th>
                                    <th width="15%">Aantal punten behaald</th>
                                    <th width="15%">Maximaal Aantal punten</th>
                                    <th width="10%">Voltooid</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($users as $user)
                                    @php
                                        $completed = \App\QuizSubmission::where('user_id', $user->id)->where('quiz_id', $quiz->id)->count() > 0 ? true : false
                                    @endphp
                                    <tr>
                                        <td>{{$user->name}}</td>
                                        <td>@if($completed)
                                                {{ \App\QuizSubmission::where('user_id', $user->id)->where('quiz_id', $quiz->id)->value('awarded_points') }}
                                            @endif
                                        </td>
                                        <td>
                                            @if($completed)
                                                {{ \App\QuizSubmission::where('user_id', $user->id)->where('quiz_id', $quiz->id)->value('max_points') }}
                                            @endif
                                        </td>
                                        <td>
                                            @if ( $completed )
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                            @endif</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection