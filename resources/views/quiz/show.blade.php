@extends('layouts.app')

@section('content')
    <form id="quiz" action="{{ route('quiz.show', ['id' => $Quiz->id]) }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-10 col-md-offset-1 c-stepper">
            <ul class="mdl-stepper mdl-stepper--linear mdl-stepper--horizontal" id="demo-stepper-linear">
                @foreach ( $questions as $category_name => $questions )
                               <li class="mdl-step">
                        <span class="mdl-step__label">
                            <span class="mdl-step__title">
                                <span class="mdl-step__title-text">{{ $category_name }}</span>
                            </span>
                        </span>
                        <div class="mdl-step__content">
                            <div class="intro_step">
                                <div class="col-md-12">
                                    <h1>{{ $Quiz->title }}</h1>
                                </div>
                                 <div class="col-md-12">
                                    <p>{!! $Quiz->description !!}</p>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="mdl-step__actions">
                            <button type="button"
                                    class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored mdl-button--raised"
                                    data-stepper-next>
                                Volgende
                            </button>
                           <span><a href="{{url('/question/pdf')}}/{{ $Quiz->id }}">Download PDF</a></span>
                        </div>
                    </li>
                    <li class="mdl-step">
                        <span class="mdl-step__label">
                            <span class="mdl-step__title">
                                <span class="mdl-step__title-text">{{ $category_name }}</span>
                            </span>
                        </span>
                        <div class="mdl-step__content">
                            @foreach($questions as $index => $question)
                                <div class="row">
                                    @php $cImages = json_decode($question->image); @endphp
                                    @if($cImages)
                                        <div class="col-md-3">
                                            <div class="row questimage">
                                                @foreach($cImages as $image)
                                                    <img id="{{ 'image-'.$question->id }}" src="/storage/quiz/{{$Quiz->id}}/{{$image}}" alt="{{ $question->imagedescription }}" height="50%" width="50%" />
                                                @endforeach
                                            </div>
                                            <div class="row">
                                                <small><i class="fa fa-search-plus" aria-hidden="true"></i>
                                                    Klik op de afbeelding om hem te vergroten</small>
                                            </div>
                                            <div class="row">
                                                {{ $question->imagedescription }}
                                            </div>
                                            @php
                                                list($width, $height) = getimagesize("storage/quiz/$Quiz->id/$cImages[0]");
                                            @endphp
                                            <script>
                                                $( document ).ready(function() {
                                                    var openPhotoSwipe = function() {
                                                        var pswpElement = document.querySelectorAll('.pswp')[0];
                                                        // build items array
                                                        var items = [
                                                            {
                                                                src: '/storage/quiz/{{$Quiz->id}}/{{$cImages[0]}}',
                                                                w: {{ $width }},
                                                                h: {{ $height }}
                                                            }
                                                        ];

                                                        // define options (if needed)
                                                        var options = {
                                                            closeEl:true,
                                                            captionEl: true,
                                                            fullscreenEl: true,
                                                            zoomEl: true,
                                                            shareEl: false,
                                                            counterEl: true,
                                                            arrowEl: true,
                                                            preloaderEl: true,
                                                            history: false,

                                                            showAnimationDuration: 0,
                                                            hideAnimationDuration: 0

                                                        };

                                                        var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
                                                        gallery.init();
                                                    };

                                                    $('#{{ "image-".$question->id }}').on('click', openPhotoSwipe);
                                                });

                                            </script>
                                        </div>
                                    @else
                                        <div class="col-md-3">

                                        </div>
                                    @endif
                                    <div class="col-md-9">
                                        <h3> {{ $question->question }}</h3>
                                        <!-- <h4>Omschrijving:</h4> -->
                                        {!! $question->description !!}
                                        <h4>Antwoordmogelijkheden:</h4>
                                        <ul>
                                            @php
                                                $answers = $question->QuestionAnswer()->get();
                                            @endphp
                                            @php
                                                $az = range('A','Z');
                                            @endphp

                                            @foreach ($answers as $index => $answer )

                                                <li>  <input type="radio" name="{{ $question->id }}"
                                                             value="{{ $answer->id }}"> {{ $az[$index]}} . {{ $answer->answer }}
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="mdl-step__actions">
                            <button type="button"
                                    class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored mdl-button--raised"
                                    data-stepper-next>
                                Volgende
                            </button>
                            <span><a href="{{url('/question/pdf')}}/{{ $Quiz->id }}">Download PDF</a></span>
                        </div>
                    </li>
                @endforeach()
                
                <li class="mdl-step">
                    <span class="mdl-step__label">
                        <span class="mdl-step__title">
                            <span class="mdl-step__title-text">Eindresultaat</span>
                        </span>
                    </span>
                    <div class="mdl-step__content outro_step">
                        <h1>Bedankt voor het maken van deze toets</h1>
                        <p>Klik op Verzenden om de toets in te sturen voor controle</p>
                    </div>
                    <div class="mdl-step__actions">
                        <button type="submit"
                                @if(Auth::user()->hasRole('beheerder')) enable @endif class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored mdl-button--raised"
                                data-stepper-next>
                            Verzenden
                        </button>
                    </div>
                </li>
            </ul>
        </div>
    </form>

    <script>

        // Upgrades all registered components found in the current DOM.
        // This is automatically called on window load.
        componentHandler.upgradeAllRegistered();

        (function () {
            var selector = '#demo-stepper-linear';
            // Select stepper container element
            var stepperElement = document.querySelector(selector);
            var Stepper;
            var steps;

            if (!stepperElement) return;

            // Get the MaterialStepper instance of element to control it.
            Stepper = stepperElement.MaterialStepper;

            if (!Stepper) {
                console.error('MaterialStepper instance is not available for selector: ' + selector + '.');
                return;
            }
            steps = stepperElement.querySelectorAll('.mdl-step');
            for (var i = 0; i < steps.length; i++) {
                // When user clicks on [data-stepper-next] button of step.
                steps[i].addEventListener('onstepnext', function (e) {
                    // {element}.MaterialStepper.next() change the state of current step to "completed"
                    // and move one step forward.
                        console.log(Stepper.getActive());
                        var inputs = $(Stepper.getActive()).find('input');
                        console.log(inputs);
                        if(inputs.length === 0) {
                            Stepper.next();
                        }
                        else {
                            var valuefound = false;
                            for (var z = 0; z < inputs.length; z++) {
                                console.log($(inputs[z]).is(':checked'));
                                if ($(inputs[z]).is(':checked')) {
                                    valuefound = true;
                                }
                            }

                            if (valuefound) {
                                Stepper.next();
                            }
                            else {
                                Stepper.error("Geen antwoord geselecteerd");
                            }
                        }
                });
            }
            // When all steps are completed this event is dispatched.
            stepperElement.addEventListener('onsteppercomplete', function (e) {
//                var toast = document.querySelector('#snackbar-stepper-complete');
//                if (!toast) return;
//                toast.MaterialSnackbar.showSnackbar({
//                    message: 'Stepper linear are completed',
//                    timeout: 4000,
//                    actionText: 'Ok'
//                });
            });



        })();
        (function () {
            var toggle = document.querySelector('[href="#stepper-linear-toggle"]');
            var snippetDirections = document.querySelector('#snippet-stepper-linear');
            var stepperElement = snippetDirections.querySelector('.mdl-stepper');
            var cssClassHorizontal = 'mdl-stepper--horizontal';
            toggle.addEventListener('click', function (event) {
                event.preventDefault();

                if (!stepperElement) return;
                if (stepperElement.classList.contains(cssClassHorizontal)) {
                    stepperElement.classList.remove(cssClassHorizontal);
                } else {
                    stepperElement.classList.add(cssClassHorizontal);
                }
            });
        })();
    </script>

    <div id="snackbar-stepper-complete" class="mdl-js-snackbar mdl-snackbar" data-upgraded=",MaterialSnackbar">
        <div class="mdl-snackbar__text"></div>
        <button class="mdl-snackbar__action" type="button" aria-hidden="true"></button>
    </div>
    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

        <!-- Background of PhotoSwipe.
             It's a separate element as animating opacity is faster than rgba(). -->
        <div class="pswp__bg"></div>

        <!-- Slides wrapper with overflow:hidden. -->
        <div class="pswp__scroll-wrap">

            <!-- Container that holds slides.
                PhotoSwipe keeps only 3 of them in the DOM to save memory.
                Don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">

                    <!--  Controls are self-explanatory. Order can be changed. -->

                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                    <button class="pswp__button pswp__button--share" title="Share"></button>

                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                    <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                    <!-- element will get class pswp__preloader--active when preloader is running -->
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                </button>

                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                </button>

                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>

            </div>

        </div>

    </div>
@stop