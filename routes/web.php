<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

/** Quiz routes here */

Route::get( '/', 'HomeController@index' )->name( 'home' );

Route::get( '/quiz/login', 'QuizLoginController@login')->name( 'quiz.login' );
Route::post( '/quiz/login', 'QuizLoginController@handleLogin' )->name( 'quiz.login' );

Route::group( [ 'middleware' => ['role:beheerder', 'auth' ] ], function () {
	Route::get( '/quiz', 'QuizController@index' )->name( 'quiz.index' );
	Route::get( '/quiz/create', 'QuizController@create')->name( 'quiz.create' );
	Route::get( '/quiz/edit/{id}', 'QuizController@edit' )->name( 'quiz.edit' );
	Route::post( '/quiz/create', 'QuizController@store' )->name( 'quiz.store' );
	Route::post( '/quiz/edit/{id}', 'QuizController@update' )->name( 'quiz.update' );

	Route::get( '/quiz/results/{id}', 'QuizController@results' )->name( 'quiz.results' );
	Route::get( '/quiz/delete/{id}', 'QuizController@delete' )->name( 'quiz.delete' );
	Route::post( '/answer/delete/{id}', 'QuizController@deleteAnswer' )->name( 'answer.delete' );
	Route::post( '/question/delete/{id}', 'QuizController@deleteQuestion' )->name( 'question.delete' );
	Route::get( '/question/pdf/{id}', 'QuizController@downloadPDF' );



	Route::get('/category', 'CategoryController@index')->name('category.index');
	Route::get('/category/create', 'CategoryController@create')->name('category.create');
	Route::post( '/category/create', 'CategoryController@store' )->name( 'category.store' );
	Route::get('/category/edit/{id}', 'CategoryController@edit')->name('category.edit');
	Route::post('/category/edit/{id}', 'CategoryController@update')->name('category.update');
	Route::get( '/category/delete/{id}', 'CategoryController@deleteCategory' )->name( 'category.delete' );

} );
Route::post('/quiz/logout', 'Auth\LoginController@logout')->name( 'custom.logout' );

Route::group( [ 'middleware' => ['role:gebruiker'] ], function () {
	Route::get( '/quiz/{id}', 'QuizController@show' )->name( 'quiz.show' );
	Route::post( '/quiz/{id}', 'QuizController@submit' )->name( 'quiz.submit' );
	Route::get( '/quiz/result/{id}', 'QuizController@quizresult' )->name( 'quiz.result' );
	Route::get( '/quiz/excel/{id}', 'QuizController@downloadExcel' );

} );



